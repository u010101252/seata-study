-- auto-generated definition
create table storage_tbl
(
    id             int auto_increment
        primary key,
    commodity_code varchar(255)  null,
    count          int default 0 null,
    constraint commodity_code
        unique (commodity_code)
)
    charset = utf8mb3;

INSERT INTO `seata-storage`.storage_tbl (id, commodity_code, count) VALUES (1, '123456', 0);


-- auto-generated definition
create table lock_table
(
    row_key        varchar(128)      not null
        primary key,
    xid            varchar(128)      null,
    transaction_id bigint            null,
    branch_id      bigint            not null,
    resource_id    varchar(256)      null,
    table_name     varchar(32)       null,
    pk             varchar(36)       null,
    status         tinyint default 0 not null comment '0:locked ,1:rollbacking',
    gmt_create     datetime          null,
    gmt_modified   datetime          null
)
    charset = utf8mb4;

create index idx_branch_id
    on lock_table (branch_id);

create index idx_status
    on lock_table (status);

create index idx_xid
    on lock_table (xid);


-- auto-generated definition
create table undo_log
(
    branch_id     bigint       not null comment 'branch transaction id',
    xid           varchar(128) not null comment 'global transaction id',
    context       varchar(128) not null comment 'undo_log context,such as serialization',
    rollback_info longblob     not null comment 'rollback info',
    log_status    int          not null comment '0:normal status,1:defense status',
    log_created   datetime(6)  not null comment 'create datetime',
    log_modified  datetime(6)  not null comment 'modify datetime',
    constraint ux_undo_log
        unique (xid, branch_id)
)
    comment 'AT transaction mode undo table' charset = utf8mb4;

create index ix_log_created
    on undo_log (log_created);

