package person.twj.accountservice.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 无
 * @since 2024-04-03
 */
@RestController
//@RequestMapping("/account")
@Slf4j
public class AccountTblController {
//用户购买商品的业务逻辑。整个业务逻辑由 3 个微服务提供支持：
//    仓储服务：对给定的商品扣除仓储数量。
//    订单服务：根据采购需求创建订单。
//    帐户服务：从用户帐户中扣除余额。
//
    /**
     * 这里是账户服务
     */
    public String pay(){
        // 下订单购买
        // 账号金额扣除
        // 订单扣除
        //

        return null;

    }

}
