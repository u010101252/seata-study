package person.twj.accountservice.service.impl;

import person.twj.accountservice.entity.AccountTbl;
import person.twj.accountservice.mapper.AccountTblMapper;
import person.twj.accountservice.service.IAccountTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 无
 * @since 2024-04-03
 */
@Service
public class AccountTblServiceImpl extends ServiceImpl<AccountTblMapper, AccountTbl> implements IAccountTblService {


}
