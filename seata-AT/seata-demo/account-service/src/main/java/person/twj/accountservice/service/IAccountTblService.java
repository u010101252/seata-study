package person.twj.accountservice.service;

import person.twj.accountservice.entity.AccountTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 无
 * @since 2024-04-03
 */
public interface IAccountTblService extends IService<AccountTbl> {

}
