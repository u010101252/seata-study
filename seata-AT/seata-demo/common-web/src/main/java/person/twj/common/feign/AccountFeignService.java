package person.twj.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import person.twj.common.ServerContant;


@FeignClient(name = ServerContant.ACCOUNT_SERVICE)
public interface AccountFeignService {
    @RequestMapping("/account/account")
     String account(@RequestParam("userId") String userId, @RequestParam("money")  int money,@RequestParam("type")  int type) ;
}
