package person.twj.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import person.twj.common.ServerContant;

@FeignClient(name = ServerContant.STORAGE_SERVICE)
public interface StorageFeignService {
    @RequestMapping(value = "/storage/storage/{commodityCode}/{count}", produces = "application/json")
    String storage(@PathVariable("commodityCode") String commodityCode,
                   @PathVariable("count") int count,
                   @RequestParam("type") int type) ;
}
