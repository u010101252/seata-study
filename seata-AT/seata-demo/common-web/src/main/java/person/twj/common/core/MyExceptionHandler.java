//package person.twj.accountservice;
//
//import io.seata.core.exception.TransactionException;
//import io.seata.tm.api.GlobalTransaction;
//import io.seata.tm.api.GlobalTransactionContext;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//@ControllerAdvice
//@Slf4j
//public class MyExceptionHandler {
//
////    @Pointcut("@annotation(io.seata.spring.annotation.GlobalTransactional)")
////    public void txAnnotation(){
////
////    }
////    @AfterThrowing(throwing = "throwable", pointcut = "txAnnotation()")
////    public void doAfterReturning(Throwable throwable) {
////        GlobalTransaction globalTransaction =
////                GlobalTransactionContext.getCurrent();
////        if (globalTransaction == null) {
////            return;
////        }
////
////        log.info("AOP------- 全局事务回滚-----xid:{}------》", globalTransaction.getXid());
////        try {
////            globalTransaction.rollback();
////        } catch (TransactionException e) {
////            e.printStackTrace();
////        }
////    }
//
//    @ExceptionHandler(value =Exception.class)
//    @ResponseBody
//    public String exceptionHandler(Exception e){
//        GlobalTransaction globalTransaction =
//                GlobalTransactionContext.getCurrent();
//        if (globalTransaction != null) {
//            log.info("AOP------- 全局事务回滚-----xid:{}------》", globalTransaction.getXid());
//            try {
//                globalTransaction.rollback();
//            } catch (TransactionException te) {
//                te.printStackTrace();
//            }
//        }
//
//        System.out.println("全局异常捕获>>>:"+e);
//        return "全局异常捕获,错误原因>>>"+e.getMessage();
//
//
//    }
//}