package person.twj.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import person.twj.common.ServerContant;

@FeignClient(name = ServerContant.ORDER_SERVICE)

public interface OrderFeignService {
    @RequestMapping(value = "/order/order")
    String order(@RequestParam("userId") String userId, @RequestParam("commodityCode") String commodityCode,
                 @RequestParam("orderCount") int orderCount,@RequestParam("type") int type);
}
