package person.twj.storageservice.service.impl;

import person.twj.storageservice.entity.StorageTbl;
import person.twj.storageservice.mapper.StorageTblMapper;
import person.twj.storageservice.service.IStorageTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 无
 * @since 2024-04-04
 */
@Service
public class StorageTblServiceImpl extends ServiceImpl<StorageTblMapper, StorageTbl> implements IStorageTblService {

}
