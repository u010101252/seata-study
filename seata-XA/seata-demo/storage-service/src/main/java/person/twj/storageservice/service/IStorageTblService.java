package person.twj.storageservice.service;

import person.twj.storageservice.entity.StorageTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 无
 * @since 2024-04-04
 */
public interface IStorageTblService extends IService<StorageTbl> {

}
