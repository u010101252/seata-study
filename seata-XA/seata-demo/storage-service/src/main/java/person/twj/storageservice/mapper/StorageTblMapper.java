package person.twj.storageservice.mapper;

import person.twj.storageservice.entity.StorageTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 无
 * @since 2024-04-04
 */
public interface StorageTblMapper extends BaseMapper<StorageTbl> {

}
