package person.twj.storageservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = {"person.twj.common.core","person.twj.storageservice"})
@MapperScan("person.twj.storageservice.mapper")
@EnableFeignClients(basePackages = "person.twj.common.feign")
public class StorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(StorageApplication.class, args);

//        MariaXaConnection mariaXaConnection = new StorageApplication();
    }

}
