package person.twj.accountservice.mapper;

import person.twj.accountservice.entity.AccountTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 无
 * @since 2024-04-03
 */
public interface AccountTblMapper extends BaseMapper<AccountTbl> {

}
