package person.twj.common;

public class ServerContant {
    public static final String ACCOUNT_SERVICE = "account-service";
    public static final String ORDER_SERVICE = "order-service";
    public static final String STORAGE_SERVICE = "storage-service";
}
