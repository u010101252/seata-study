package person.twj.common.core;

import com.zaxxer.hikari.HikariDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

//@Configuration
public class DataSourceConfig {

    @Bean("dataSource")
    public DataSource dataSource(DataSourceProperties properties) {
        // 使用Seata的代理数据源
        HikariDataSource dataSource = new HikariDataSource();
        // 设置Hikari的属性
        dataSource.setDriverClassName(properties.getDriverClassName());
        dataSource.setUsername(properties.getUsername());
        dataSource.setPassword(properties.getPassword());
        dataSource.setJdbcUrl(properties.getUrl());
        return new DataSourceProxy(dataSource);
    }
}