//package person.twj.accountservice;
//
//import io.seata.core.exception.TransactionException;
//import io.seata.tm.api.GlobalTransaction;
//import io.seata.tm.api.GlobalTransactionContext;
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.annotation.AfterThrowing;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.stereotype.Component;
//
//@Slf4j
//@Aspect
//@Component
//public class ATTransactionalHandler {
//
//    @Pointcut("@annotation(io.seata.spring.annotation.GlobalTransactional)")
//    public void txAnnotation(){
//
//    }
//
//    @AfterThrowing(throwing = "throwable", pointcut = "txAnnotation()")
//    public void doAfterReturning(Throwable throwable) {
//        GlobalTransaction globalTransaction =
//        GlobalTransactionContext.getCurrent();
//        if (globalTransaction == null) {
//            return;
//        }
//
//        log.info("AOP------- 全局事务回滚-----xid:{}------》", globalTransaction.getXid());
//        try {
//            globalTransaction.rollback();
//        } catch (TransactionException e) {
//            e.printStackTrace();
//        }
//    }
//}