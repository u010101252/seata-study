package person.twj.orderservice.service.impl;

import person.twj.orderservice.entity.OrderTbl;
import person.twj.orderservice.mapper.OrderTblMapper;
import person.twj.orderservice.service.IOrderTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 无
 * @since 2024-04-04
 */
@Service
public class OrderTblServiceImpl extends ServiceImpl<OrderTblMapper, OrderTbl> implements IOrderTblService {

}
