package person.twj.storageservice.mapper;

import person.twj.storageservice.entity.StorageFreezeTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 无
 * @since 2024-04-06
 */
public interface StorageFreezeTblMapper extends BaseMapper<StorageFreezeTbl> {

}
