package person.twj.storageservice.feign.client;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.seata.core.context.RootContext;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import person.twj.storageservice.entity.StorageFreezeTbl;
import person.twj.storageservice.entity.StorageTbl;
import person.twj.storageservice.service.IStorageFreezeTblService;
import person.twj.storageservice.service.IStorageTblService;

@Service
@Slf4j
public class TccStorageServiceImpl implements TccStorageService{
    @Autowired
    @Qualifier("storageTblServiceImpl")
    private IStorageTblService iStorageTblService;
    @Autowired
    @Qualifier("storageFreezeTblServiceImpl")
    private IStorageFreezeTblService iStorageFreezeTblService;
    @Override
    @Transactional
    public String storage(String commodityCode, int count, int type) {
        String storageId = "1";
        // 库存-count
        log.info("Storage Service Begin ... xid: " + RootContext.getXID());
        StorageTbl storage = iStorageTblService.getById(storageId);
        log.info("原始storage: " + JSON.toJSONString(storage));
        UpdateWrapper wrapper = new UpdateWrapper();
        wrapper.setSql("count = count - {0} where commodity_code = {1}",
                count, commodityCode );

        boolean result = iStorageTblService.update(wrapper);
        log.info("Storage Service End ... ");

        if (result ) {

            boolean res = iStorageFreezeTblService.save(StorageFreezeTbl.builder()
                            .xid(RootContext.getXID())
                            .freezeCount(count)
                            .status(StorageFreezeTbl.Status.TRY)
                            .storageId(storageId)
                    .build());
            if (res) {
                if(type==3){
                    throw new RuntimeException("type=3");
                }
                log.info("执行 try 成功");
                return "SUCCESS";
            }


        }
        return "FAIL";
    }

    @Override
    public boolean commit(BusinessActionContext actionContext) {
        log.info("执行 commit 成功");
        return iStorageFreezeTblService.removeById(actionContext.getXid());
    }

    @Override
    public boolean rollback(BusinessActionContext actionContext) {

        StorageFreezeTbl freeze = iStorageFreezeTblService.getById(actionContext.getXid());


        UpdateWrapper wrapper = new UpdateWrapper();
        wrapper.setSql("count = count + {0} where commodity_code = {1}",
                new Object[]{freeze.getFreezeCount(),
                        actionContext.getActionContext("commodityCode").toString()
        });

        boolean result = iStorageTblService.update(wrapper);
        if(result){
            freeze.setFreezeCount(0);
            freeze.setStatus(StorageFreezeTbl.Status.CANSEL);
            boolean status = iStorageFreezeTblService.updateById(freeze);
            if(status){
                log.info("执行rollback成功");
                return true;
            }
        }

        return false;
    }
}
