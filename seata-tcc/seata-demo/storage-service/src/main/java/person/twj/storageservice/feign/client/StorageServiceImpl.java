package person.twj.storageservice.feign.client;

import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import person.twj.common.feign.StorageFeignService;

@Slf4j
@RestController
@RequestMapping("/storage")
public class StorageServiceImpl implements StorageFeignService {


    @Autowired
    @Qualifier("tccStorageServiceImpl")
    private TccStorageService tccStorageService;
    @Override
    @GetMapping(value = "/storage/{commodityCode}/{count}", produces = "application/json")
    @GlobalTransactional
    public String storage(@PathVariable("commodityCode") String commodityCode,
                          @PathVariable("count") int count,
                          @RequestParam("type") int type) {
        return tccStorageService.storage(commodityCode, count, type);

    }
}
