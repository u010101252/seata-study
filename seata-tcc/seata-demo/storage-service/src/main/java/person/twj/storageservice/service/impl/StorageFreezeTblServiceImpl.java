package person.twj.storageservice.service.impl;

import person.twj.storageservice.entity.StorageFreezeTbl;
import person.twj.storageservice.mapper.StorageFreezeTblMapper;
import person.twj.storageservice.service.IStorageFreezeTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 无
 * @since 2024-04-06
 */
@Service
public class StorageFreezeTblServiceImpl extends ServiceImpl<StorageFreezeTblMapper, StorageFreezeTbl> implements IStorageFreezeTblService {

}
