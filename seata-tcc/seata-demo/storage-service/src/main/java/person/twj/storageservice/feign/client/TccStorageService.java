package person.twj.storageservice.feign.client;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;



@LocalTCC
public interface TccStorageService {
    @TwoPhaseBusinessAction(name = "storage", commitMethod = "commit", rollbackMethod = "rollback",useTCCFence = true)
    String storage(@BusinessActionContextParameter("commodityCode") String commodityCode,
                 @BusinessActionContextParameter("count") int count,
                 @BusinessActionContextParameter("type") int type
    ) ;


    boolean commit(BusinessActionContext actionContext);

    boolean rollback(BusinessActionContext actionContext);
}