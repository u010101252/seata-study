## 1. 第一步、进入数据库
进入mariadb数据库
```
[root]# mariadb -uroot -p
```



## 2. 第二步、创建用户，创建库
--创建用户
```sql
create user 'seata'@'%' identified by 'seata'; 
```

--创建库
```sql
create database `seata-account`; 
--分配权限
grant all privileges on `seata-account`.* to seata with grant option;

--创建库
create database `seata-order`; 
--分配权限
grant all privileges on `seata-order`.* to seata with grant option;

--创建库
create database `seata-storage`; 
--分配权限
grant all privileges on `seata-storage`.* to seata with grant option;
```


这里多增加了
```sql
--创建库
create database `seata-business`; 
--分配权限
grant all privileges on `seata-business`.* to seata with grant option;
```

## 3. 第三步、导入表数据
最后，把当前目录下的：
```
./seata-account.sql
./seata-order.sql
./seata-storage.sql
./seata-business.sql
```

分别执行在不同的数据库中 seata-account、seata-order、seata-storage、seata-business

## 4. 第四步、启动nacos、seata
启动nacos、seata

## 5. 第五步，启动服务

配置business 增加数据库连接seata-business。

启动服务，下面是各个服务所分配的端口
```
gateway    8085
account    8080
order        8081
storage    8083
business    8082
```
## 6. 第六步，游览器访问
在postman中，输入访问地址：
```
http://localhost:8082/seata/feign?type=4
```
type的意思：报错地方，用来测试事务是否回滚
```
type=
0 不报错
1 account报错
2 order报错
3 storage报错
4 business报错
```

## 7. 第七步，备注信息
```
商品id: 123456
用户id： 128
```

业务执行顺序
```
库存-1
余额-1
order-1
```


  
