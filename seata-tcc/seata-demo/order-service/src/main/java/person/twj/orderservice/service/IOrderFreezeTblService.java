package person.twj.orderservice.service;

import person.twj.orderservice.entity.OrderFreezeTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 无
 * @since 2024-04-06
 */
public interface IOrderFreezeTblService extends IService<OrderFreezeTbl> {

}
