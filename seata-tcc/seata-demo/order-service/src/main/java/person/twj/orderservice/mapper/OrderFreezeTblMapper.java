package person.twj.orderservice.mapper;

import person.twj.orderservice.entity.OrderFreezeTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 无
 * @since 2024-04-06
 */
public interface OrderFreezeTblMapper extends BaseMapper<OrderFreezeTbl> {

}
