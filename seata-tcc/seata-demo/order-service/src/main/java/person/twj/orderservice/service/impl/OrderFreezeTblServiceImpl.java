package person.twj.orderservice.service.impl;

import person.twj.orderservice.entity.OrderFreezeTbl;
import person.twj.orderservice.mapper.OrderFreezeTblMapper;
import person.twj.orderservice.service.IOrderFreezeTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 无
 * @since 2024-04-06
 */
@Service
public class OrderFreezeTblServiceImpl extends ServiceImpl<OrderFreezeTblMapper, OrderFreezeTbl> implements IOrderFreezeTblService {

}
