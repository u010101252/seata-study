package person.twj.orderservice.feign.client;

import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import person.twj.common.feign.OrderFeignService;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderFeignServiceImpl implements OrderFeignService {
    @Autowired
    @Qualifier("tccOrderServiceImpl")
    private TccOrderService tccOrderService;
    @Override
    @GetMapping(value = "/order")
    @GlobalTransactional
    public String order(@RequestParam("userId") String userId,
                        @RequestParam("commodityCode") String commodityCode,
                        @RequestParam("orderCount") int orderCount,
                        @RequestParam("type") int type) {
        return tccOrderService.order(userId, commodityCode, orderCount, type);
    }


}
