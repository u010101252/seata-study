package person.twj.orderservice.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 无
 * @since 2024-04-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("order_freeze_tbl")
@Builder
public class OrderFreezeTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "xid", type = IdType.AUTO)
    private String xid;

    private String orderId;

    private Integer freezeCount;

    private Integer freezeMoney;

    private Integer status;

    public static abstract class Status {
        public static final int TRY = 1;
        public static final int COMMIT = 2;
        public static final int CANSEL = 3;
    }
}
