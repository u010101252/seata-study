package person.twj.orderservice.feign.client;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;


@LocalTCC
public interface TccOrderService {
    @TwoPhaseBusinessAction(name = "order", commitMethod = "commit", rollbackMethod = "rollback",useTCCFence = true)
    String order(@BusinessActionContextParameter("userId") String userId,
                   @BusinessActionContextParameter("commodityCode") String commodityCode,
                   @BusinessActionContextParameter("orderCount") int orderCount,
                   @BusinessActionContextParameter("type") int type
                 ) ;


    boolean commit(BusinessActionContext actionContext);

    boolean rollback(BusinessActionContext actionContext);
}
