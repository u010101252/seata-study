package person.twj.orderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = {"person.twj.common.core", "person.twj.orderservice"})
//@MapperScan("person.twj")
@EnableFeignClients(basePackages = "person.twj.common.feign")
public class ServiceWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceWebApplication.class, args);
    }

}
