package person.twj.accountservice.feign.client;

import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import person.twj.common.feign.AccountFeignService;

@Slf4j
@RestController
@RequestMapping("/account")
//@LocalTCC
public class AccountFeignServiceImpl implements AccountFeignService {

    @Autowired
    @Qualifier("tccAccountServiceImpl")
    private TccAccountService tccAccountService;

    @Override
    @RequestMapping("/account")
    @GlobalTransactional
    public String account(@RequestParam("userId") String userId,
                          @RequestParam("money") int money,
                          @RequestParam("type") int type) {
        return tccAccountService.account(userId, money, type);
    }


}
