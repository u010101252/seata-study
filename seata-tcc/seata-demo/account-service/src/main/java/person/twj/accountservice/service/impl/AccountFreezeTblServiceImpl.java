package person.twj.accountservice.service.impl;

import person.twj.accountservice.entity.AccountFreezeTbl;
import person.twj.accountservice.mapper.AccountFreezeTblMapper;
import person.twj.accountservice.service.IAccountFreezeTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 无
 * @since 2024-04-06
 */
@Service
public class AccountFreezeTblServiceImpl extends ServiceImpl<AccountFreezeTblMapper, AccountFreezeTbl> implements IAccountFreezeTblService {

}
