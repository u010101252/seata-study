package person.twj.accountservice.service;

import person.twj.accountservice.entity.AccountFreezeTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 无
 * @since 2024-04-06
 */
public interface IAccountFreezeTblService extends IService<AccountFreezeTbl> {

}
