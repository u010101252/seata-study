package person.twj.accountservice.feign.client;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface TccAccountService {
    @TwoPhaseBusinessAction(name = "account", commitMethod = "commit", rollbackMethod = "rollback",useTCCFence = true)
    String account(@BusinessActionContextParameter("userId") String userId,
                          @BusinessActionContextParameter("money") int money,
                          @BusinessActionContextParameter("type") int type) ;

    boolean commit(BusinessActionContext actionContext);

    boolean rollback(BusinessActionContext actionContext);
}
